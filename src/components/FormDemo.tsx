import { Paper } from "@material-ui/core";
import React from "react";
import { schema } from "../schema-form/constants";
import SchemaForm from "../schema-form/SchemaForm";

function FormDemo() {
  return (
    <Paper style={{ padding: 10 }}>
      <SchemaForm
        schema={schema}
        onSubmit={(value) => console.log("submit", value)}
        onChange={(value) => console.log("onChange", value)}
        // hideSubmitButton
        hiddenField={{ fuck: "12312" }}
      />
    </Paper>
  );
}

export default FormDemo;
