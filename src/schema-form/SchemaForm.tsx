import React from "react";
import { FormProvider, useForm } from "react-hook-form";
import SchemaView from "./SchemaView";
import { getFieldForm, ISchemaForm, some } from "./utils";

type Mode = "edit" | "create";

interface Props {
  schema?: ISchemaForm;
  fieldName?: string;
  onSubmit?: (data: some) => void;
  formData?: some;
  hiddenField?: some;
  mode?: Mode;
  onChange?: (value: some) => void;
  hideSubmitButton?: boolean;
}

SchemaForm.defaultProps = {
  fieldName: "",
  hideSubmitButton: false,
};

function SchemaForm(props: Props) {
  const {
    schema,
    fieldName,
    onSubmit,
    formData,
    onChange,
    hideSubmitButton,
    hiddenField,
    ...rest
  } = props;
  const { defaultValues } = getFieldForm(undefined, schema);
  const methods = useForm({
    defaultValues: formData || defaultValues,
  });
  const { watch } = methods;
  const onSubmitForm = (data: any) => {
    let dataMerge = { ...data, ...hiddenField };
    dataMerge = schema?.changeDataBeforeSubmit ? schema?.changeDataBeforeSubmit(dataMerge) : dataMerge;
    onSubmit && onSubmit(dataMerge);
  };

  React.useEffect(() => {
    onChange && onChange(watch());
  }, [methods, onChange, watch]);

  return (
    <FormProvider {...methods}>
      <form
        onSubmit={methods.handleSubmit(onSubmitForm)}
        style={{ display: "flex", flexDirection: "column" }}
      >
        <SchemaView
          schema={schema}
          fieldName={fieldName}
          showSubmitButton={!hideSubmitButton}
        />
      </form>
    </FormProvider>
  );
}

export default SchemaForm;
