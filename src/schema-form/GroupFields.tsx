import { Grid, Paper, Typography } from "@material-ui/core";
import React from "react";
import SchemaElement from "./SchemaElement";
import { ElementProps, mergeFieldName, some } from "./utils";

interface Props {
  schema: some;
  fieldName?: string;
}

function GroupFields(props: Props) {
  const { schema, fieldName } = props;

  const { fields, xs, paper, title, className } = schema;
  const content = React.useMemo(() => {
    let tmpContent = fields?.map((value: some, index: number) => {
      return (
        <SchemaElement
          key={index}
          {...(value as ElementProps)}
          fieldName={mergeFieldName(value?.key_element, fieldName)}
        />
      );
    });

    if (tmpContent.length) {
      tmpContent = (
        <Grid container spacing={2}>
          {tmpContent}
        </Grid>
      );
    }

    if (paper || title) {
      tmpContent = (
        <Paper variant="outlined" style={{ padding: 10 }} className={className}>
          <Typography variant="h5">{title}</Typography>
          {tmpContent}
        </Paper>
      );
    }

    tmpContent = (
      <Grid item xs={xs || 12}>
        {tmpContent}
      </Grid>
    );
    return tmpContent;
  }, [className, fieldName, fields, paper, title, xs]);

  return <>{content}</>;
}

export default GroupFields;
