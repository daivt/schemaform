import { get } from "lodash";
import { ReactNode } from "react";
import { RegisterOptions } from "react-hook-form";

export type some = { [key: string]: any };

export type ElementType =
  | "text-field"
  | "select"
  | "auto-complete"
  | "switch"
  | "checkbox"
  | "multiple-checkbox"
  | "radio"
  | "multiple-radio"
  | "array"
  | "section"
  | ReactNode;

type xsType =
  | boolean
  | "auto"
  | 1
  | 2
  | 3
  | 4
  | 5
  | 6
  | 7
  | 8
  | 9
  | 10
  | 11
  | 12
  | undefined;

export interface ElementProps {
  type: ElementType;
  register?: RegisterOptions;
  xs?: xsType;
  xs_section?: number;
  label?: string;
  title?: string;
  defaultValue?: any;
  unregister?: boolean;
  propsWrapper?: some;
}

export type SchemaType = {
  [key: string]: ElementProps;
};

export interface ISchemaForm {
  fields?: SchemaType;
  ui?: (values: some, methods: any) => some[] | some[];
  layout?: ReactNode;
  changeDataBeforeSubmit?: (values: some) => some;
}

export const mergeFieldName = (name: string, key?: string, index?: number) => {
  if (key && index) {
    return `${key}[${index}].${name}`;
  }
  if (key) {
    return `${key}.${name}`;
  }
  if (typeof index === "number") {
    return `${name}[${index}]`;
  }
  return name;
};

const getDefaultValues = (groupFields, methods) => {
  const defaultValues = groupFields.reduce((value, current, index) => {
    return {
      ...value,
      ...current.fields.reduce((val, cur, idx) => {
        if (cur.type === "reactNode") {
          return val;
        }
        if (cur.type === "section") {
          return {
            ...val,
            [cur.key_element]: getFieldForm(methods, cur.schema).defaultValues,
          };
        }
        if (cur.defaultValue) {
          return { ...val, [cur.key_element]: cur.defaultValue };
        }
        return val;
      }, {}),
    };
  }, {});
  return defaultValues;
};

export const getFieldForm = (
  methods?: any,
  schema?: some,
  showSubmitButton?: boolean
) => {
  if (!schema) {
    return { groupFields: [] };
  }
  const fields =
    typeof schema?.fields === "function"
      ? schema?.fields(methods ? methods?.getValues() : {}, methods)
      : schema?.fields;
  const ui =
    typeof schema?.ui === "function"
      ? schema?.ui(methods ? methods?.getValues() : {}, methods)
      : schema?.ui;

  let groupFields = ui
    ? ui
        .reduce((value: some[], current: some, index: number) => {
          const flatValue = value.reduce(
            (val: some[], cur: some, idx: number) => {
              return [
                ...val,
                ...get(cur, "fields", [])
                  .map(
                    (v) =>
                      // v.type !== "submitButton" ? v.key_element : undefined
                      v.key_element
                  )
                  .filter(Boolean),
              ];
            },
            []
          );
          return [
            ...value,
            {
              ...current,
              fields: get(current, "fields", [])
                .filter((val) => !flatValue.includes(val))
                .map((val: string) => {
                  const field = get(fields, val);
                  return field && { ...field, key_element: val };
                })
                .filter(Boolean),
            },
          ];
        }, [])
        .filter(Boolean)
    : [
        {
          fields: Object.entries(fields).map(([key, value]) => {
            return { key_element: key, ...(value as object) };
          }),
        },
      ];

  if (showSubmitButton) {
    groupFields = [
      ...groupFields,
      {
        id: "submitButton",
        fields: [
          {
            type: "submitButton",
            propsWrapper: {
              style: {
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              },
            },
          },
        ],
      },
    ];
  }
  const defaultValues = getDefaultValues(groupFields, methods);

  return { groupFields, defaultValues };
};
