import { Checkbox, FormControlLabel } from "@material-ui/core";
import React from "react";
import { ControllerRenderProps, InputState } from "react-hook-form";
import { some } from "../../utils";

export interface PropsCheckBoxElement extends some {
  propsRender: ControllerRenderProps;
  inputState: InputState;
}

export default function CheckBoxElement(props: PropsCheckBoxElement) {
  const { propsRender, inputState, label, labelPlacement, ...rest } = props;
  const { name, value, onChange } = propsRender;

  return (
    <FormControlLabel
      control={
        <Checkbox
          checked={value}
          onChange={(e) => onChange(e.target.checked)}
          name={name}
          {...rest}
        />
      }
      label={label}
      labelPlacement={labelPlacement}
    />
  );
}
