import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
} from "@material-ui/core";
import React from "react";
import { ControllerRenderProps, InputState } from "react-hook-form";
import { some } from "../../utils";

export interface PropsMultipleRadioElement extends some {
  propsRender: ControllerRenderProps;
  inputState: InputState;
}

export default function MultipleRadioElement(props: PropsMultipleRadioElement) {
  const {
    propsRender,
    inputState,
    options,
    label,
    row = false,
    onChange: onChangeProps,
    className,
    ...rest
  } = props;
  const { name, value, onChange } = propsRender;

  const handleChange = (event) => {
    let tmp = event.target.value;
    if (typeof value === "number") {
      tmp = parseInt(tmp, 10);
    }
    onChange(tmp);
    onChangeProps && onChangeProps(tmp);
  };

  return (
    <FormControl component="fieldset" className={className}>
      {label && <FormLabel component="legend">{label}</FormLabel>}
      <RadioGroup
        {...rest}
        aria-label={name}
        name={name}
        value={String(value)}
        row={row}
        onChange={handleChange}
      >
        {options?.map(
          (
            { label: labelOption = "", value: valueOption, ...one }: some,
            index: number
          ) => {
            return (
              <FormControlLabel
                key={index}
                control={<Radio />}
                label={labelOption}
                value={String(valueOption)}
                {...one}
              />
            );
          }
        )}
      </RadioGroup>
    </FormControl>
  );
}
