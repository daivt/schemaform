import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel
} from "@material-ui/core";
import React from "react";
import { ControllerRenderProps, InputState } from "react-hook-form";
import { some } from "../../utils";

export interface PropsMultipleCheckBoxElement extends some {
  propsRender: ControllerRenderProps;
  inputState: InputState;
}

export default function MultipleCheckBoxElement(
  props: PropsMultipleCheckBoxElement
) {
  const {
    propsRender,
    inputState,
    options,
    label,
    labelPlacement,
    onChange: onChangeProps,
    row,
    className,
    ...rest
  } = props;
  const { name, value, onChange } = propsRender;

  const handleChange = (item) => {
    const isContain = value.includes(item);
    let tmp = value;
    if (isContain) {
      tmp = tmp.filter((v) => v !== item);
    } else {
      tmp = [...tmp, item];
    }
    onChange(tmp);
    onChangeProps && onChangeProps(tmp);
  };

  return (
    <FormControl component="fieldset" className={className}>
      {label && <FormLabel component="legend">{label}</FormLabel>}
      <FormGroup aria-label="position" row={row}>
        {options?.map(
          (
            { label: labelOption = "", value: valueOption, ...one }: some,
            index: number
          ) => {
            return (
              <FormControlLabel
                key={index}
                control={
                  <Checkbox
                    checked={value.includes(valueOption)}
                    onChange={() => handleChange(valueOption)}
                    {...rest}
                  />
                }
                label={labelOption}
                {...one}
              />
            );
          }
        )}
      </FormGroup>
    </FormControl>
  );
}
