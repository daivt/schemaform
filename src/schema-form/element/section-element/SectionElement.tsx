import { Grid, Paper } from "@material-ui/core";
import React from "react";
import { some } from "../../utils";
import SchemaView from "../../SchemaView";

export interface PropsSectionElement extends some {
  name: string;
  schema?: some;
}

export default function SectionElement(props: PropsSectionElement) {
  const { name, schema, ...rest } = props;
  return (
    <Paper variant="outlined" style={{ padding: 15 }}>
      <Grid container spacing={2}>
        <SchemaView fieldName={name} schema={schema} {...rest} />
      </Grid>
    </Paper>
  );
}
