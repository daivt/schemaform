import { Checkbox, FormControlLabel, Radio } from "@material-ui/core";
import React from "react";
import { ControllerRenderProps, InputState } from "react-hook-form";
import { some } from "../../utils";

export interface PropsRadioElement extends some {
  propsRender: ControllerRenderProps;
  inputState: InputState;
}

export default function RadioElement(props: PropsRadioElement) {
  const {
    propsRender,
    inputState,
    label,
    labelPlacement,
    onChange: onChangeProps,
    ...rest
  } = props;
  const { name, value, onChange } = propsRender;

  return (
    <FormControlLabel
      control={
        <Radio
          {...rest}
          checked={value}
          onClick={(e) => {
            onChange(!value);
            onChangeProps && onChangeProps(!value );
          }}
          name={name}
        />
      }
      label={label}
      labelPlacement={labelPlacement}
    />
  );
}
