import React from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import {Controller, Control, useFormContext} from "react-hook-form";
import {isEqual} from 'lodash'
import { some } from "../../utils";

interface PropsAutoCompleteElement extends some {
    name: string,
}

export default function AutoCompleteElement(props: PropsAutoCompleteElement) {
    const {name} = props
    const {control} = useFormContext();
    return (
        <Controller
            render={(propsRender: any) => (
                <Autocomplete
                    {...propsRender}
                    getOptionLabel={(option: some) => option.label}
                    getOptionSelected={(option: some, value: some) =>
                        isEqual(option, value)
                    }
                    renderInput={params => (
                        <TextField
                            {...params}
                            label={props.label}
                            placeholder={props.placeholder}
                            variant="standard"
                        />
                    )}
                    fullWidth
                    {...props}
                    onChange={(_, data) => {
                        propsRender.onChange(data)
                        props.onChange(data)
                    }}

                />
            )}
            control={control}
            name={name}
            defaultValue={null}
        />
    );
}
