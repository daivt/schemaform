import { Button, Grid, IconButton, Paper } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import React from "react";
import { useFieldArray, useFormContext } from "react-hook-form";
import SchemaView from "../../SchemaView";
import { mergeFieldName,some } from "../../utils";

export interface PropsArrayElement extends some {
  name: string;
  schema?: some;
}

export default function ArrayElement(props: PropsArrayElement) {
  const { name, schema, xs_section, disableCloseBtn, ...rest } = props;
  const { control, register } = useFormContext();
  const { fields, append, prepend, remove, swap, move, insert } = useFieldArray(
    {
      control,
      name,
    }
  );

  React.useEffect(() => {
    if (fields.length === 0) {
      append({});
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Paper variant="outlined" style={{ padding: 15 }}>
      <Grid container spacing={2}>
        {fields.map((item, index) => {
          const disabled =
            typeof disableCloseBtn === "function"
              ? disableCloseBtn(item, index)
              : disableCloseBtn;

          return (
            <Grid key={item.id} item xs={xs_section || 12}>
              <Paper
                variant="outlined"
                style={{ padding: 15, position: "relative" }}
              >
                {!disabled && (
                  <IconButton
                    size="small"
                    onClick={() => {
                      remove(index);
                    }}
                    style={{
                      position: "absolute",
                      zIndex: 999,
                      top: 4,
                      right: 4,
                    }}
                  >
                    <CloseIcon />
                  </IconButton>
                )}
                <SchemaView
                  fieldName={mergeFieldName(name, undefined, index)}
                  schema={schema}
                  {...rest}
                />
              </Paper>
            </Grid>
          );
        })}
      </Grid>
      <Button
        variant="contained"
        onClick={() => {
          append({});
        }}
      >
        append
      </Button>
    </Paper>
  );
}
