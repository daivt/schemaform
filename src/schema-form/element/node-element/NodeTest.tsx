import React from "react";
import { TextField } from "@material-ui/core";
import { ControllerRenderProps, InputState } from "react-hook-form";
import { some } from "../../utils";

interface Props extends some {
  propsRender: ControllerRenderProps;
}

function NodeTest(props: Props) {
  const {
    propsRender,
    title,
    label,
    defaultValue,
    inputState,
    renderProps,
    ...rest
  } = props;
  const { name, value = "", onChange } = propsRender;
  // const {setValue} = useFormContext();
  return (
    <div>
      <TextField
        // {...rest}
        fullWidth
        value={value}
        title={title}
        label={label}
        onChange={(e) => {
          onChange(e.target.value);
        }}
      />
    </div>
  );
}

export default NodeTest;
