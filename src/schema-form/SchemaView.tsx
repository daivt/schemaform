import { Grid } from "@material-ui/core";
import React from "react";
import { useFormContext } from "react-hook-form";
import GroupFields from "./GroupFields";
import SchemaElement from "./SchemaElement";
import { ElementProps, getFieldForm, mergeFieldName, some } from "./utils";

interface Props extends some {
  schema?: some;
  fieldName?: string;
  showSubmitButton?: boolean;
}

SchemaView.defaultProps = {
  fieldName: "",
  showSubmitButton: false,
};

function SchemaView(props: Props) {
  const { schema, fieldName, spacing, showSubmitButton, ...rest } = props;
  const methods = useFormContext();
  const { groupFields } = getFieldForm(methods, schema, showSubmitButton);

  const getListElement = React.useMemo(() => {
    const tmp: any[] = [];
    groupFields?.forEach((value: some, index: any) => {
      tmp.push(
        <GroupFields key={index} schema={value} fieldName={fieldName} />
      );
    });
    return tmp;
  }, [fieldName, groupFields]);

  const getFieldsElement = React.useMemo(() => {
    return groupFields?.reduce((value: some, current: some, index: number) => {
      return {
        ...value,
        [current?.id || index]: current?.fields.reduce(
          (val: some, cur: some, idx: number) => {
            return {
              ...val,
              [cur?.key_element]: (
                <SchemaElement
                  {...(cur as ElementProps)}
                  fieldName={mergeFieldName(cur?.key_element, fieldName)}
                  key={`${index}-${idx}`}
                  rawElement
                />
              ),
            };
          },
          {}
        ),
      };
    }, {});
  }, [fieldName, groupFields]);

  if (!schema) {
    return null;
  }

  if (schema?.layout) {
    return React.createElement(schema.layout, {
      fields: getFieldsElement,
      methods,
      ...rest,
    });
  }
  return (
    <Grid container spacing={spacing || 3}>
      {getListElement}
    </Grid>
  );
}

export default SchemaView;
