import { UseFormMethods } from "react-hook-form";
import { some } from "./utils";

interface Props extends some {
    fields: some;
    methods: UseFormMethods;
}

function LayoutSchema(props: Props) {
    const {fields, methods, getListElement} = props;

    const temp = (Object.values(fields) as any)
        .reduce((v, c, i) => {
            return [...v, ...Object.values(c)];
        }, [])
        .flat(Infinity);

  return temp;
}

export default LayoutSchema;
