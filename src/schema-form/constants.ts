import NodeTest from "./element/node-element/NodeTest";
import LayoutSchema from "./LayoutSchema";
import { ISchemaForm, SchemaType, some } from "./utils";

// @ts-ignore
export const fakeSchema: SchemaType = (values: any, methods) => {
  return {
    name: {
      type: "text-field",
      label: "ahihi",
      onChange: (e: any) => {
        // console.log("FICK", e.target.value);
      },
      defaultValue: "121",
      xs: 3,
    },
    multipleCheckbox: {
      type: "multiple-checkbox",
      label: "multiple-checkbox",
      options: [
        { value: 1, label: "1" },
        { value: 2, label: "2" },
        { value: 3, label: "3" },
      ],
      defaultValue: [1],
      row: true,
      register: {
        validate: (value) => value?.length > 0 || "error message",
      },
    },
    checkbox: {
      type: "checkbox",
      label: "ahihi",
      defaultValue: true,
    },
    radio: {
      type: "radio",
      label: "asdsadsa",
      // defaultValue: true,
    },
    radioMultiple: {
      type: "multiple-radio",
      label: "asdsadsa",
      options: [
        { value: 1, label: "1" },
        { value: 2, label: "2" },
        { value: 3, label: "3" },
      ],
      defaultValue: 1,
      row: true,
    },
    age: {
      type: "auto-complete",
      label: "ahihi",
      options: [
        { value: 1, label: "1" },
        { value: 2, label: "2" },
        { value: 3, label: "3" },
      ],
      onChange: (e: any) => {
        // console.log(e);
      },

      xs: 3,
    },
    node_new: {
      type: NodeTest,
      label: "node_new",
      title: "title",
      defaultValues: "12312321",
      // register: {
      //   required: true,
      //   pattern: {
      //     value: /\d+/,
      //     message: "This input is number only.",
      //   },
      //   // validate: (value: string) => (value === "1" ? "chahahsda" : "dmmdmdmd"),
      //   minLength: {
      //     value: 11,
      //     message: "This input must exceed 10 characters",
      //   },
      // },
      xs: 3,
    },
    test: {
      type: "array",
      xs: 6,
      xs_section: 6,
      //   disableCloseBtn: (value, index) => true,
      // disableCloseBtn: true,
      schema: {
        fields: {
          chan: {
            type: "text-field",
            label: "chan wa",
            onChange: (e: any) => {
              // console.log(e);
            },
            register: {
              required: "error message",
            },
            defaultValue: "121312",
            xs: 6,
          },
          age: {
            type: "text-field",
            label: "chan wa",
            onChange: (e: any) => {
              // console.log(e);
            },
            register: {
              required: "error message",
            },
            xs: 6,
          },
        },
      },
    },
    submit: {
      type: "submitButton",
      variant: "outlined",
      propsWrapper: {
        style: {
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        },
      },
    },
    test2: {
      type: "section",
      xs: 6,
      xs_section: 6,
      //   disableCloseBtn: (value, index) => true,
      // disableCloseBtn: true,
      schema: {
        fields: {
          chan: {
            type: "text-field",
            label: "section-text-field",
            onChange: (e: any) => {
              // console.log(e);
            },
            register: {
              required: "error message",
            },
            defaultValue: "121312",
            xs: 6,
          },
          checkbox: {
            type: "checkbox",
            label: "ahihi",
            defaultValue: true,
            xs: 6,
            register: {
              validate: (value) => value || "error message",
            },
          },
          switch: {
            type: "switch",
            label: "ahihi",
            defaultValue: true,
            xs: 6,
            labelPlacement: "start",
          },
        },
      },
    },
  } as SchemaType;
};
const fakeUI = (values: some, methods: any) => {
  return [
    {
      id: "default",
      // paper: true,
      // xs: 6,
      title: "Box 1",
      fields: [
        "name",
        "checkbox",
        "multipleCheckbox",
        "radio",
        "radioMultiple",
        "switch",
        "test",
        "test2",
        "node_new",
        // "submit",
      ],
    },
    {
      id: "defaults",
      // paper: true,
      // xs: 6,
      title: "Box 2",
      fields: ["name"],
    },
  ];
};
export const schema: ISchemaForm = {
  fields: fakeSchema,
  ui: fakeUI,
  layout: LayoutSchema,
};
