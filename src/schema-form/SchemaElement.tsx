import { Button, FormHelperText, Grid, TextField } from "@material-ui/core";
import { get, pickBy } from "lodash";
import _ from "lodash/fp";
import React from "react";
import { Controller, useFormContext } from "react-hook-form";
import ArrayElement from "./element/array-element/ArrayElement";
import AutoCompleteElement from "./element/autocomplete/AutoCompleteElement";
import CheckBoxElement from "./element/checkbox/CheckBoxElement";
import MultipleCheckBoxElement from "./element/multiple-checkbox/MultipleCheckBoxElement";
import MultipleRadioElement from "./element/multiple-radio/MultipleRadioElement";
import RadioElement from "./element/radio/RadioElement";
import SectionElement from "./element/section-element/SectionElement";
import SwitchElement from "./element/switch/SwitchElement";
import { ElementProps, ElementType } from "./utils";

interface Props extends ElementProps {
  fieldName: string;
  rawElement?: boolean;
  submitButton?: any;
}

SchemaElement.defaultProps = {};

function SchemaElement(props: Props) {
  const {
    type = undefined,
    unregister,
    fieldName,
    xs,
    register: registerElement,
    rawElement,
    propsWrapper,
    ...rest
  } = props;

  const methods = useFormContext();
  const { register, control, getValues, setValue, errors } = methods;

  const getElement = React.useMemo(() => {
    let element;
    if (!type) {
      return [];
    }
    switch (type as ElementType) {
      case "text-field":
        element = (
          <TextField
            inputRef={register(registerElement)}
            fullWidth
            {...rest}
            name={fieldName}
            error={get(errors, fieldName)}
          />
        );
        break;
      case "checkbox":
        element = (
          <Controller
            name={fieldName}
            control={control}
            rules={registerElement}
            defaultValue={rest?.defaultValue || false}
            render={(propsRender, inputState) => (
              <CheckBoxElement
                propsRender={propsRender}
                inputState={inputState}
                {...rest}
              />
            )}
          />
        );
        break;
      case "multiple-checkbox":
        element = (
          <Controller
            name={fieldName}
            control={control}
            rules={registerElement}
            defaultValue={rest?.defaultValue || []}
            render={(propsRender, inputState) => (
              <MultipleCheckBoxElement
                propsRender={propsRender}
                inputState={inputState}
                {...rest}
              />
            )}
          />
        );
        break;
      case "radio":
        element = (
          <Controller
            name={fieldName}
            control={control}
            rules={registerElement}
            defaultValue={rest?.defaultValue || false}
            render={(propsRender, inputState) => (
              <RadioElement
                propsRender={propsRender}
                inputState={inputState}
                {...rest}
              />
            )}
          />
        );
        break;
      case "multiple-radio":
        element = (
          <Controller
            name={fieldName}
            control={control}
            rules={registerElement}
            defaultValue={rest?.defaultValue}
            render={(propsRender, inputState) => (
              <MultipleRadioElement
                propsRender={propsRender}
                inputState={inputState}
                {...rest}
              />
            )}
          />
        );
        break;
      case "switch":
        element = (
          <Controller
            name={fieldName}
            control={control}
            rules={registerElement}
            defaultValue={rest?.defaultValue || false}
            render={(propsRender, inputState) => (
              <SwitchElement
                propsRender={propsRender}
                inputState={inputState}
                {...rest}
              />
            )}
          />
        );
        break;
      case "auto-complete":
        element = <AutoCompleteElement {...rest} name={fieldName} />;
        break;
      case "array":
        element = <ArrayElement {...rest} name={fieldName} />;
        break;
      case "section":
        element = <SectionElement {...rest} name={fieldName} />;
        break;
      case "submitButton":
        element = (
          <Button
            type="submit"
            variant="contained"
            color="secondary"
            disableElevation
            {...rest}
          >
            Submit
          </Button>
        );
        break;
      default:
        try {
          element = unregister ? (
            React.createElement(type as any, rest)
          ) : (
            <Controller
              name={fieldName}
              defaultValue={rest.defaultValue}
              render={(propsRender, inputState) => (
                <>
                  {React.createElement(type as any, {
                    propsRender: propsRender,
                    inputState: inputState,
                    ...rest,
                  })}
                </>
              )}
              rules={registerElement}
            />
          );
        } catch (e) {}
        break;
    }
    return element;
  }, [
    control,
    errors,
    fieldName,
    register,
    registerElement,
    rest,
    type,
    unregister,
  ]);

  const errorMessage = React.useMemo(() => {
    const tmp = pickBy(get(errors, fieldName), (value, key) => {
      return key !== "ref";
    });
    const type_tmp = tmp?.type;
    const message = tmp?.message;
    delete tmp.type;
    delete tmp.message;
    return { ...tmp, message: message || type_tmp };
  }, [errors, fieldName]);

  const content = React.useMemo(
    () => (
      <>
        {getElement}
        <FormHelperText style={{ minHeight: 20 }} error component="div">
          {errorMessage
            ? _.entries(errorMessage).map(
                ([type_error, message]: [string, unknown]) =>
                  typeof message === "string" &&
                  (message ? type_error !== "type" : true) && (
                    <div key={type_error}>
                      {message}
                      <br />
                    </div>
                  )
              )
            : null}
        </FormHelperText>
      </>
    ),
    [errorMessage, getElement]
  );
  if (rawElement) {
    return <div {...propsWrapper}>{content}</div>;
  }
  return (
    <Grid item xs={xs || 12} {...propsWrapper}>
      {content}
    </Grid>
  );
}

export default SchemaElement;
